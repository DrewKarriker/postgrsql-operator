# This postgreSQL Operator is from [An Introduction to Devops - a deep dive into Kubernetes](https://drewlearns.com/devops-and-kubernetes/#pgo)

1.  Setup storage `kubectl create -f storage.yml`

0. Setup Operator `./quickstart.sh && ./set-path.sh`

0. After these commands you'll need to logout and login again.

0. Run `kubectl get pods -n pgo` to get the operator name and paste it in the next command

0. Port forward the postgres-operator `kubectl port-forward -n pgo postgres-operator-xxx-yyy 8443:8443`

0. Test command - `pgo version`

0. Create cluster - `pgo create cluster mycluster`

0. Show secrets - `pgo show cluster mycluster`

0. Connect to psql - `pgo show user mycluster` then `kubectl run -it --rm --image=postgres:10.4 psql-client -- psql -h mycluster.pgo -U testuser -W postgres`

0. Create read replic - `pgo scale mycluster`

0. Manually failover

        pgo failover mycluster --query
        pgo failover mycluster --target=mycluster-xxx
        kubectl get pgtasks mycluster-failover -o yaml

